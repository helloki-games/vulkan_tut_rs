use std::ptr;
use ash::vk;
use crate::cfg;
use crate::queue_family_support::QueueFamilySupport;
use crate::swap_chain_support_details::SwapChainSupportDetails;
use crate::utils::img_utils;
use crate::vulkan_app::SurfaceData;




pub struct SwapchainData {
    pub swapchain: vk::SwapchainKHR,
    pub swapchain_loader: ash::extensions::khr::Swapchain,

    pub _imgs: Vec<vk::Image>,
    pub img_views: Vec<vk::ImageView>,

    pub surface_format: vk::SurfaceFormatKHR,
    pub extent: vk::Extent2D,
}


impl SwapchainData {
    pub fn new(instance: &ash::Instance, phys_device: vk::PhysicalDevice, device: &ash::Device, surface_data: &SurfaceData) -> SwapchainData {
        let swap_chain_support = SwapChainSupportDetails::new(phys_device, surface_data);

        let surface_format = swap_chain_support.choose_swap_surface_format();
        let swap_present_mode = swap_chain_support.choose_swap_present_mode();
        let extent = swap_chain_support.choose_swap_extend(cfg::WINDOW_WIDTH, cfg::WINDOW_HEIGHT);
        let swap_img_count = swap_chain_support.choose_img_count();

        let queue_family_support = QueueFamilySupport::new(instance, phys_device, surface_data);
        let is_single_queue = queue_family_support.single_queue();
        let queue_indices: Vec<_> = queue_family_support.indices().into_iter().collect();

        let create_info = vk::SwapchainCreateInfoKHR {
            s_type: vk::StructureType::SWAPCHAIN_CREATE_INFO_KHR,
            p_next: ptr::null(),
            flags: vk::SwapchainCreateFlagsKHR::empty(),
            surface: surface_data.surface,
            min_image_count: swap_img_count,
            image_format: surface_format.format,
            image_color_space: surface_format.color_space,
            image_extent: extent,
            image_array_layers: 1,  // always 1, unless for stereoscopic 3D applications
            image_usage: vk::ImageUsageFlags::COLOR_ATTACHMENT,     // directly render to the images in the swap-chain

            image_sharing_mode: if is_single_queue { vk::SharingMode::EXCLUSIVE } else { vk::SharingMode::CONCURRENT },
            queue_family_index_count: if is_single_queue { 0 } else { 2 },
            p_queue_family_indices: if is_single_queue { ptr::null() } else { queue_indices.as_ptr() },

            pre_transform: swap_chain_support.capabilities.current_transform,
            composite_alpha: vk::CompositeAlphaFlagsKHR::OPAQUE, // blend with other windows in the window system?
            present_mode: swap_present_mode,
            clipped: vk::TRUE, // ignore color of pixels, that are obscured by other windows
            old_swapchain: vk::SwapchainKHR::null(),
        };

        let swapchain_loader = ash::extensions::khr::Swapchain::new(instance, device);
        let swapchain = unsafe {
            swapchain_loader
                .create_swapchain(&create_info, None)
                .expect("failed to create swapchain")
        };

        let imgs = unsafe { swapchain_loader.get_swapchain_images(swapchain).unwrap() };
        let img_views = img_utils::create_img_views(device, &imgs, 1, surface_format.format, vk::ImageAspectFlags::COLOR);



        println!("swapchain created with {} images...", imgs.len());

        SwapchainData {
            swapchain,
            swapchain_loader,
            _imgs: imgs,
            img_views,

            surface_format,
            extent,
        }
    }
}


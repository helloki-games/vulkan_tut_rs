extern crate core;

mod vulkan_app;
mod utils;
mod device_analysis;
mod swap_chain_support_details;
mod queue_family_support;
mod app;
mod cfg;
mod vertex;


use crate::vulkan_app::VulkanApp;


fn main() {
    println!("app started");

    let event_loop = winit::event_loop::EventLoop::new();
    let app = VulkanApp::new(&event_loop);

    app.main_loop(event_loop);
}

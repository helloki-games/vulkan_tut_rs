use std::collections::HashSet;
use ash::vk;
use crate::vulkan_app::SurfaceData;


#[derive(Debug, Default)]
pub struct QueueFamilySupport {
    pub graphics_family: Option<u32>,
    pub present_family: Option<u32>,
    pub transfer_family: Option<u32>,
}

impl QueueFamilySupport {
    pub fn new(instance: &ash::Instance, phys_device: vk::PhysicalDevice, surface_data: &SurfaceData) -> Self {
        let props = unsafe { instance.get_physical_device_queue_family_properties(phys_device) };


        let mut result = QueueFamilySupport::default();
        for (idx , prop) in props.iter().enumerate() {
            let idx = idx as u32;

            if prop.queue_flags.contains(vk::QueueFlags::GRAPHICS) {
                result.graphics_family = Some(idx);
            } else if prop.queue_flags.contains(vk::QueueFlags::TRANSFER) {
                result.transfer_family = Some(idx);
            }

            if result.present_family.is_none() {
                let present_is_supported = unsafe {
                    surface_data.surface_loader
                        .get_physical_device_surface_support(phys_device, idx, surface_data.surface)
                        .unwrap()
                };

                if present_is_supported {
                    result.present_family = Some(idx);
                }
            }

            if result.is_complete() { break; }
        }


        result
    }

}

impl QueueFamilySupport {
    pub fn single_queue(&self) -> bool {
        self.graphics_family.unwrap() == self.present_family.unwrap()
    }

    pub fn indices(&self) -> HashSet<u32> {
        [self.graphics_family, self.present_family, self.transfer_family].into_iter()
            .flatten()
            .collect()
    }

    pub fn is_complete(&self) -> bool {
        self.graphics_family.is_some() &&
            self.present_family.is_some() &&
            self.transfer_family.is_some()
    }
}

pub fn print_queue_families(instance: &ash::Instance, device: vk::PhysicalDevice) {
    let props = unsafe { instance.get_physical_device_queue_family_properties(device) };

    for (idx, prop) in props.iter().enumerate() {
        println!("\t> queue: {}", idx);

        if prop.queue_flags.contains(vk::QueueFlags::GRAPHICS) { println!("\t\t> GRAPHICS-QUEUE"); }
        if prop.queue_flags.contains(vk::QueueFlags::COMPUTE) { println!("\t\t> COMPUTE-QUEUE"); }
        if prop.queue_flags.contains(vk::QueueFlags::TRANSFER) { println!("\t\t> TRANSFER-QUEUE"); }
        if prop.queue_flags.contains(vk::QueueFlags::SPARSE_BINDING) { println!("\t\t> SPARSE-BINDING-QUEUE"); }
    }
}


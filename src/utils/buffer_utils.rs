use std::ptr;
use ash::vk;


pub fn begin_single_time_commands(device: &ash::Device, cmd_pool: vk::CommandPool) -> vk::CommandBuffer {
    let alloc_info = vk::CommandBufferAllocateInfo {
        s_type: vk::StructureType::COMMAND_BUFFER_ALLOCATE_INFO,
        p_next: ptr::null(),
        command_pool: cmd_pool,
        level: vk::CommandBufferLevel::PRIMARY,
        command_buffer_count: 1,
    };

    let cmd_buffer = unsafe {
        device.allocate_command_buffers(&alloc_info)
            .expect("failed to create single-time-command-buffer")
            [0]
    };

    let begin_info = vk::CommandBufferBeginInfo {
        s_type: vk::StructureType::COMMAND_BUFFER_BEGIN_INFO,
        p_next: ptr::null(),
        flags: vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
        p_inheritance_info: ptr::null(),
    };

    unsafe {
        device.begin_command_buffer(cmd_buffer, &begin_info)
            .expect("failed to begin command-buffer");
    }

    cmd_buffer
}



pub fn end_single_time_commands(device: &ash::Device, cmd_pool: vk::CommandPool, cmd_buffer: vk::CommandBuffer, queue: vk::Queue) {
    unsafe {
        device.end_command_buffer(cmd_buffer)
            .expect("failed to end single-time-command-buffer");
    }

    let submit_info = vk::SubmitInfo {
        s_type: vk::StructureType::SUBMIT_INFO,
        p_next: ptr::null(),
        wait_semaphore_count: 0,
        p_wait_semaphores: ptr::null(),
        p_wait_dst_stage_mask: ptr::null(),
        command_buffer_count: 1,
        p_command_buffers: &cmd_buffer,
        signal_semaphore_count: 0,
        p_signal_semaphores: ptr::null(),
    };

    unsafe {
        device.queue_submit(queue, &[submit_info], vk::Fence::null())
            .expect("failed to submit single-time-command-buffer");
        device.queue_wait_idle(queue).unwrap();
        device.free_command_buffers(cmd_pool, &[cmd_buffer]);
    }
}

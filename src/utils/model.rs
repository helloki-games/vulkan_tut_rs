use std::path::Path;
use tobj;

use crate::vertex::Vertex;

pub struct Model {
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u32>,
}

impl Model {
    pub fn new(path: &Path) -> Self {
        let (vertices, indices) = Self::load_model_data(path);

        println!(
            "model created with '{}' vertices and '{}' indices!",
            vertices.len(),
            indices.len()
        );

        Self { vertices, indices }
    }

    // PERF: optimize vertex-count
    fn load_model_data(path: &Path) -> (Vec<Vertex>, Vec<u32>) {
        let load_opt = tobj::LoadOptions::default();

        let (models, _) = tobj::load_obj(path, &load_opt).expect("failed to load model");

        let mut vertices = vec![];
        let mut indices = vec![];

        for m in models {
            let mesh = &m.mesh;

            if mesh.texcoords.is_empty() {
                panic!("no texture coordinates found for model");
            }

            let vert_count = mesh.positions.len() / 3;

            for i in 0..vert_count {
                vertices.push(Vertex {
                    pos: [
                        mesh.positions[i * 3],
                        mesh.positions[i * 3 + 1],
                        mesh.positions[i * 3 + 2],
                        1.0,
                    ],
                    color: [2.0, 2.0, 2.0, 1.0],
                    tex_coord: [
                        mesh.texcoords[i * 2],
                        1.0 - mesh.texcoords[i * 2 + 1], // OBJ files expect v / y: 0 to be at the bottom
                    ],
                });
            }

            indices = mesh.indices.clone();
        }

        (vertices, indices)
    }
}
